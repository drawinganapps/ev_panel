import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';
import 'package:tesla_panel_control/routes/AppRoutes.dart';
import 'package:tesla_panel_control/views/widgets/charging_status_widget.dart';

class ChargingPage extends StatelessWidget {
  const ChargingPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            margin: EdgeInsets.only(
                top: heightSize * 0.04, bottom: heightSize * 0.02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () => context.go(AppRoutes.HOME),
                  child: const Icon(Icons.arrow_back_ios),
                ),
                Text('Charging', style: TextStyle(color: ColorHelper.white)),
                Container()
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: ColorHelper.lightDark,
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: widthSize * 0.03),
                        child: Icon(Icons.ev_station,
                            color: ColorHelper.secondary),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: heightSize * 0.02),
                            child: Text('Sanur Station',
                                style: TextStyle(
                                    color: ColorHelper.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18)),
                          ),
                          Text('Jl. Bypass Ngurah Rai - Sanur, Denpasar, Bali.',
                              style: TextStyle(
                                  color: ColorHelper.white.withOpacity(0.8),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10))
                        ],
                      ),
                    ],
                  ),
                  Icon(Icons.info_outline, color: ColorHelper.grey),
                ],
              ),
            ),
          ),
          Image.asset('assets/images/wuling.png',
              width: widthSize, height: heightSize * 0.35),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Charging',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18)),
                Text('72%',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18))
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: heightSize * 0.02, bottom: heightSize * 0.02),
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: widthSize * 0.02),
                      child: Icon(
                        Icons.offline_bolt,
                        color: ColorHelper.primary,
                        size: 20,
                      ),
                    ),
                    Text('Fast charging',
                        style: TextStyle(
                            color: ColorHelper.white.withOpacity(0.8),
                            fontWeight: FontWeight.w600,
                            fontSize: 10))
                  ],
                ),
                Text('127 mil remaining distance',
                    style: TextStyle(
                        color: ColorHelper.white.withOpacity(0.8),
                        fontWeight: FontWeight.w600,
                        fontSize: 10))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: const ChargingStatusWidget(),
          )
        ],
      ),
    );
  }
}
