import 'package:flutter/material.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';
import 'package:tesla_panel_control/views/widgets/battery_status_widget.dart';
import 'package:tesla_panel_control/views/widgets/charging_station_widget.dart';
import 'package:tesla_panel_control/views/widgets/weather_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
              height: heightSize * 0.05,
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              margin: EdgeInsets.only(
                  top: heightSize * 0.04, bottom: heightSize * 0.02),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Ready to have a ride today?',
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w600)),
                      Text('Wuling Air EV',
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w600)),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(right: widthSize * 0.05),
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                            color: ColorHelper.lightDark,
                            shape: BoxShape.circle),
                        clipBehavior: Clip.antiAlias,
                        child: Image.asset('assets/images/profile.jpg',
                            fit: BoxFit.cover),
                      ),
                    ),
                  )
                ],
              )),
          Container(
            margin: EdgeInsets.only(bottom: heightSize * 0.05),
            child: Image.asset('assets/images/wuling.png',
                width: widthSize, height: heightSize * 0.34),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: const ChargingStationWidget(),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            margin: EdgeInsets.only(
                top: heightSize * 0.01, bottom: heightSize * 0.01),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const BatteryStatusWidget(),
                SizedBox(
                  height: widthSize * 0.44,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const WeatherWidget(),
                      Container(
                        width: widthSize * 0.43,
                        height: heightSize * 0.07,
                        padding: EdgeInsets.all(widthSize * 0.03),
                        decoration: BoxDecoration(
                            color: ColorHelper.lightDark,
                            borderRadius: BorderRadius.circular(15)),
                        child: Row(
                          children: [
                            const Icon(Icons.lock_open),
                            Container(
                              margin: EdgeInsets.only(left: widthSize * 0.03),
                              child: Text('Unlocked',
                                  style: TextStyle(
                                      color: ColorHelper.white,
                                      fontWeight: FontWeight.w600)),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
