import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';
import 'package:tesla_panel_control/views/pages/charging_page.dart';

class ChargingScreen extends StatelessWidget {
  const ChargingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    return Scaffold(
      body: const ChargingPage(),
      bottomNavigationBar: Container(
        height: heightSize * 0.1,
        padding: EdgeInsets.only(
            left: widthSize * 0.05,
            right: widthSize * 0.05,
            top: heightSize * 0.015,
            bottom: heightSize * 0.015),
        child: Container(
            decoration: BoxDecoration(
                color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(20)),
            width: widthSize,
            child: Center(
              child: Text('Stop Charging',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w600)),
            )),
      ),
    );
  }
}
