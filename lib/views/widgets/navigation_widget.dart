import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';
import 'package:tesla_panel_control/routes/AppRoutes.dart';

class NavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const NavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    Widget menuItem(IconData icon, String title, bool isSelected) {
      return Container(
        decoration: const BoxDecoration(shape: BoxShape.circle),
        padding: EdgeInsets.all(heightSize * 0.015),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(icon,
                size: 25,
                color: isSelected ? ColorHelper.primary : ColorHelper.white),
            Text(title,
                style: TextStyle(color: ColorHelper.white, fontSize: 11))
          ],
        ),
      );
    }

    return Container(
      height: heightSize * 0.1,
      decoration: BoxDecoration(color: ColorHelper.lightDark),
      padding: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () => context.go(AppRoutes.HOME),
            child: menuItem(Icons.home_rounded, 'Home', selectedMenu == 0),
          ),
          menuItem(Icons.ev_station, 'Station', selectedMenu == 1),
          GestureDetector(
            onTap: () => context.go(AppRoutes.CHARGING),
            child: menuItem(Icons.analytics_outlined, 'Status', selectedMenu == 2),
          ),
          menuItem(Icons.person, 'Profile', selectedMenu == 2),
        ],
      ),
    );
  }
}
