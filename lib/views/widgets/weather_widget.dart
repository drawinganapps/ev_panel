import 'package:flutter/material.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';

class WeatherWidget extends StatelessWidget {
  const WeatherWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.all(widthSize * 0.03),
      height: heightSize * 0.12,
      width: widthSize * 0.44,
      decoration: BoxDecoration(color: ColorHelper.lightDark, borderRadius: BorderRadius.circular(15)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Icon(Icons.nightlight),
              Text('Denpasar, Bali',
                  style: TextStyle(color: ColorHelper.white, fontSize: 12))
            ],
          ),
          Text(
            '18\'C',
            style: TextStyle(
                color: ColorHelper.white,
                fontSize: 18,
                fontWeight: FontWeight.w600),
          ),
          Text(
            'Day 24\'C - Night 18\'C',
            style: TextStyle(
                color: ColorHelper.white.withOpacity(0.8),
                fontSize: 10,
                fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
