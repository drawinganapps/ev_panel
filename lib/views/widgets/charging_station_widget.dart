import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';

class ChargingStationWidget extends StatelessWidget {
  const ChargingStationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.18,
      padding: EdgeInsets.all(widthSize * 0.03),
      decoration: BoxDecoration(
          color: ColorHelper.lightDark,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: widthSize * 0.4,
            padding: EdgeInsets.only(
              right: widthSize * 0.05
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Charging Station',
                    style: TextStyle(color: ColorHelper.white, fontSize: 16)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: widthSize * 0.03),
                      child: Icon(Icons.ev_station, color: ColorHelper.secondary),
                    ),
                    Text('Sanur Station',
                        style: TextStyle(color: ColorHelper.white))
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Jl. Bypass Ngurah Rai - Sanur',
                        style:
                        TextStyle(color: ColorHelper.white, fontSize: 9)),
                    Text('Denpasar, Bali',
                        style:
                        TextStyle(color: ColorHelper.white, fontSize: 9)),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03, top: heightSize * 0.01, bottom: heightSize * 0.01),
                      decoration: BoxDecoration(
                          color: ColorHelper.primary,
                          borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text('4.3 Mile', style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 10
                      )),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03, top: heightSize * 0.01, bottom: heightSize * 0.01),
                      decoration: BoxDecoration(
                          color: ColorHelper.primary,
                          borderRadius: BorderRadius.circular(15)
                      ),
                      child: Text('62 Min', style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 10
                      )),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10)
            ),
            width: widthSize * 0.4,
            clipBehavior: Clip.antiAlias,
            child: Image.asset('assets/images/map.PNG', fit: BoxFit.cover),
          )
        ],
      ),
    );
  }
}
