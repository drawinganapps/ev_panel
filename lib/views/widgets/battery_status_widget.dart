import 'package:flutter/cupertino.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';

class BatteryStatusWidget extends StatelessWidget {
  const BatteryStatusWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      width: widthSize * 0.44,
      height: widthSize * 0.44,
      padding: EdgeInsets.all(widthSize * 0.03),
      decoration: BoxDecoration(
          color: ColorHelper.lightDark,
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: heightSize * 0.006),
                child:
                    Text('Battery', style: TextStyle(color: ColorHelper.white)),
              ),
              Text('1h 54m 5s Remaining',
                  style: TextStyle(
                      color: ColorHelper.white.withOpacity(0.8), fontSize: 10))
            ],
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                height: heightSize * 0.12,
                width: widthSize * 0.15,
                padding: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: ColorHelper.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(),
                        Container(
                          height: heightSize * 0.07,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    ColorHelper.green,
                                    ColorHelper.greenLight,
                                  ]),
                              borderRadius: BorderRadius.circular(10)),
                        )
                      ],
                    ),
                    Positioned(
                        right: widthSize * 0.030,
                        top: heightSize * 0.05,
                        child: Text('72%',
                            style: TextStyle(
                                color: ColorHelper.dark,
                                fontWeight: FontWeight.w600)))
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('72.3 Kwh',
                      style: TextStyle(
                          color: ColorHelper.white.withOpacity(0.8),
                          fontSize: 12)),
                  Container(
                    margin: EdgeInsets.only(top: heightSize * 0.008),
                    padding: EdgeInsets.only(top: heightSize * 0.008),
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(
                                color: ColorHelper.white.withOpacity(0.5)))),
                    child: Text('272 KM',
                        style: TextStyle(
                            color: ColorHelper.white.withOpacity(0.8),
                            fontWeight: FontWeight.w600,
                            fontSize: 16)),
                  )
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
