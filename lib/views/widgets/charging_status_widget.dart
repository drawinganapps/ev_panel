import 'package:flutter/material.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';

class ChargingStatusWidget extends StatelessWidget {
  const ChargingStatusWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    Widget batteryStatus(int chargingStatus) {
      return Container(
        width: widthSize * 0.15,
        height: heightSize * 0.15,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                chargingStatus == 2 ? ColorHelper.green : ColorHelper.white,
                chargingStatus == 2 ? ColorHelper.greenLight : ColorHelper.white,
              ]),
        ),
        child: chargingStatus == 1 ? Icon(Icons.bolt_rounded, color: ColorHelper.yellow, size: 50) : Container(),
      );
    }

    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: ColorHelper.lightDark,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('2h 36m', style: TextStyle(
                  color: ColorHelper.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18
              )),
              Text('Charging time - 4,79 voltage', style: TextStyle(
                  color: ColorHelper.white.withOpacity(0.8),
                  fontWeight: FontWeight.w600,
                  fontSize: 10
              ))
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: heightSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                batteryStatus(2),
                batteryStatus(2),
                batteryStatus(2),
                batteryStatus(1),
                batteryStatus(0),
              ],
            ),
          )
        ],
      ),
    );
  }
}
