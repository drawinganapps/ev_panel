import 'package:flutter/material.dart';

class ColorHelper {
  static Color primary = const Color.fromRGBO(51, 107, 254, 1);
  static Color secondary = const Color.fromRGBO(255, 100, 100, 1);
  static Color dark = const Color.fromRGBO(15, 15, 15, 1);
  static Color lightDark = const Color.fromRGBO(33, 35, 37, 1);
  static Color white = const Color.fromRGBO(251,251,251, 1);
  static Color yellow = const Color.fromRGBO(249, 185, 6, 1);
  static Color grey = const Color.fromRGBO(128, 128, 128, 1);
  static Color green = const Color.fromRGBO(124, 207, 106, 1);
  static Color greenDark = const Color.fromRGBO(49, 172, 70, 1);
  static Color greenLight = const Color.fromRGBO(151, 231, 99, 1);
}
