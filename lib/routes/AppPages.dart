import 'package:go_router/go_router.dart';
import 'package:tesla_panel_control/routes/AppRoutes.dart';
import 'package:tesla_panel_control/views/screens/charging_screen.dart';
import 'package:tesla_panel_control/views/screens/home_screen.dart';

class AppPage {
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.CHARGING,
        builder: (context, state) => const ChargingScreen(),
      )
    ],
  );
}
