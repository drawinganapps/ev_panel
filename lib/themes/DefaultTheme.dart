import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tesla_panel_control/helper/color_helper.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: ColorHelper.dark,
    scaffoldBackgroundColor: ColorHelper.dark,
    highlightColor: ColorHelper.dark,
    splashColor: ColorHelper.dark,
    textTheme: GoogleFonts.arimoTextTheme().copyWith(
    ),
);
